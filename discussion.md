Problems
========

1. Axes should have a rank=1 (dimensions tag of `AXISNAME`) while at the same time axes could
span more than one dimension (`AXISNAME_indices` can be a list).

2. `AXISNAME_indices` makes the `@axes` attribute ambiguous because the number of strings in
`@axes` must be equal to the rank of `data`.

AXISNAME_indices
================

This was the situation before `AXISNAME_indices`

```
NXroot
   NXentry
     NXdata
       @signal=“data”
       @axes=”x”,“y”,”z”
       data: float[10,20,30]
       x: float[10]
       y: float[20]
       z: float[30]
```

and when a dimension does not have a coordinate

```
NXroot
   NXentry
     NXdata
       @signal=“data”
       @axes=”x”,“y”,”.”
       data: float[10,20,30]
       x: float[10]
       y: float[20]
```

The limitations of this approach that lead to the introduction of `AXISNAME_indices`:

1. coordinates can only form a regular (but not necessarily even-spaces) N-dimensional grid
2. each dimension can only have one coordinate

This is an example that violates both: each points of `data` has `x`, `y` and `z` coordinates
(all axes span three dimensions) and a `time` coordinate (also spanning three dimensions)

```
NXroot
   NXentry
     NXdata
       @signal=“data”
       @axes=”?”,“?”,”?”   <--- WHAT SHOULD THIS BE?
       @x_indices=0,1,2
       @y_indices=0,1,2
       @z_indices=0,1,2
       @time_indices=0,1,2
       data: float[10,20,30]
       x: float[10,20,30]
       y: float[20,20,30]
       z: float[10,20,30]
       time: float[10,20,30]
```

In this example we have a five dimensional dataset. The `x` and `z` coordinates span dimensions
0 and 4, the `y` coordinate spans dimension 3 and dimensions 1 and 2 have no coordinates. The
`time` dimension spans all five dimensions.

```
NXroot
   NXentry
     NXdata
       @signal=“data”
       @axes=”?”,“?”,”?”,”?”,”?”   <--- WHAT SHOULD THIS BE?
       @x_indices=0,5
       @y_indices=4
       @z_indices=0,5
       @time_indices=0,1,2,3,4
       data: float[10,20,30,40,50]
       x: float[10,50]
       y: float[20]
       z: float[10,50]
       time: float[10,20,30,40,50]
```

Possible solution:

1. when `AXISNAME_indices` are present, the `@axes` should just be used as a list of axes
without a specific order. So the number of strings in `@axes` is not necessarily equal to the rank of `data`
and the ”.” in `@axes` is meaningless.

2. when `AXISNAME_indices` are not present, the number of strings in `@axes` must be equal to the rank of `data`
and the ”.” in `@axes` is needed for missing coordinates. The position in `@axes` essentially defines its
`AXISNAME_indices`. For example this

```
NXroot
   NXentry
     NXdata
       @signal=“data”
       @axes=”x”,“.”,”z”   <--- order is important
       data: float[10,20,30]
       x: float[10]
       z: float[30]
```

is equivalent to this

```
NXroot
   NXentry
     NXdata
       @signal=“data”
       @axes=”x”,”z”   <--- order is NOT important
       @x_indices=0,2
       @z_indices=0,2
       data: float[10,20,30]
       x: float[10]
       z: float[30]
```