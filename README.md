# NXdata axes

Improve documentation of *NXdata axes*:

https://hdf5.gitlab-pages.esrf.fr/nexus/nxdata_axes/classes/base_classes/NXdata.html

Submitted to the official *NeXus standard*:

https://github.com/nexusformat/definitions/pull/1213
